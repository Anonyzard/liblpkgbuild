#!/bin/env python3
import os

class lpkgbuild():
    def __init__(self):
        self.destdir = "/tmp/"
        self.main_dir = "/opt/Loc-OS-LPKG/lpkgbuild"
        self.installed = self.main_dir + "/remove"
        self.arch = os.uname()[-1]
        if self.arch == "x86_64":
            self.arch = "64"
        else:
            self.arch = "32"
        self.list = "https://gitlab.com/loc-os_linux/lpkgbuild/-/raw/main/lpkgbuild-"+self.arch+".list"
        self.remote = "https://gitlab.com/loc-os_linux/lpkgbuild/-/raw/main/"+self.arch+"/"
    def install(self, name):
        """Download tar file, extract to /tmp and execute /tmp/$pkg/install.sh"""
        with tarfile.open(file, 'r') as lpkgbuild:
            lpkg.extractall(path=self.destdir)
        os.system(f"sh /tmp/{name}/install.sh")
    def remove(self, name):
        """Execute ${self.installed}/$pkg"""
        os.system(f"{self.installed}/{name}")
    def is_installed(self, name):
        """Exists file on {self.installed}?"""
        if os.path.exists(f"{self.installed}/{name}"):
            return True
        else:
            return False
    def update_list(self):
        """Download and override list"""
        return False
        
